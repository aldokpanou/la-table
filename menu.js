const menuToggle = document.getElementById('menuToggle');
const menuContainer = document.getElementById('menuContainer');
const closeBtn = document.getElementById('closeBtn');

let isMenuOpen = false;

function toggleMenu() {
  if (isMenuOpen) {
    menuContainer.classList.remove('visible');
    setTimeout(function() {
      menuContainer.style.display = 'none';
    }, 300);
    isMenuOpen = false;
  } else {
    menuContainer.style.display = 'flex';
    setTimeout(function() {
      menuContainer.classList.add('visible');
    }, 10);
    isMenuOpen = true;
  }
}

menuToggle.addEventListener('click', toggleMenu);
closeBtn.addEventListener('click', toggleMenu);